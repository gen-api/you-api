package com.genapi.youapi.service;

import com.genapi.youapi.constants.RegexConstants;
import com.genapi.youapi.dto.ModelFieldDTO;
import com.genapi.youapi.enums.FieldTypeEnum;
import com.genapi.youapi.repository.BodyMongoRepository;
import com.genapi.youapi.repository.ListsMongoRepository;
import com.mongodb.BasicDBObject;
import com.mongodb.DBRef;
import lombok.AllArgsConstructor;
import org.bson.Document;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class BodyPreviewService {

    private final BodyMongoRepository bodyMongoRepository;
    private final ListsMongoRepository listsMongoRepository;

    public Document save(Integer id, Document value, List<ModelFieldDTO> model) throws IOException {
        Document doc = toDOC(value, model);
        bodyMongoRepository.save(id, doc);
        return doc;
    }

    public List<Object> saveList(Integer id, List<Object> docArr, String type) {
        List<Object> list = toArrayDoc(docArr, type);
        listsMongoRepository.save(id, list);
        return list;
    }

    public boolean deleteBody(Integer id) {
        return bodyMongoRepository.deleteById(id);
    }

    public boolean deleteBody(List<Integer> ids) {
        return bodyMongoRepository.deleteById(ids);
    }

    public boolean deleteList(Integer id) {
        return listsMongoRepository.deleteById(id);
    }

    public boolean deleteList(List<Integer> ids) {
        return listsMongoRepository.deleteById(ids);
    }

    public Document getPreviewById(int id) {
        return getPreviewById(id, 5);
    }

    public Document getById(int id) {
        return bodyMongoRepository.findOne(id);
    }

    public Document getByIdForBody(int id) {
        Document getedDoc = bodyMongoRepository.findOneById(id);
        Document result = new Document();

        getedDoc.remove("_id");

        getedDoc.forEach((name, value) -> {
            if(value instanceof DBRef) {
                result.put(name, ((DBRef) value).getId());
            } else {
                result.put(name, value);
            }
        });

        return result;
    }

    public List<Object> getByIdForList(int id) {
        Document getedDoc = listsMongoRepository.findOneById(id);
        List<Object> items = (List<Object>) getedDoc.get("LIST");
        List<Object> result = new ArrayList<>();

        items.forEach(value -> {
            if(value instanceof DBRef) {
                result.add(((DBRef) value).getId());
            } else {
                result.add(value);
            }
        });

        return result;
    }

    public List<Object> getPreviewListById(int id) {
        return this.getPreviewListById(id, 5);
    }

    private List<Object> getPreviewListById(int id, int index) {
        if(index <= 0) return null;

        Document getedDoc = listsMongoRepository.getMappedDocById(id);
        if(getedDoc == null) return null;
        return (List<Object>) getedDoc.get("LIST");
//        Document getedDoc = listsMongoRepository.findOne(query);
//        if(getedDoc == null) return null;
//        List<Object> arr = new ArrayList<>();
//        List<Object> items = (List<Object>) getedDoc.get("LIST");
//
//        items.forEach(item -> {
//            if(item instanceof DBRef) {
//                if(((DBRef) item).getCollectionName().equals("lists"))
//                    arr.add(getPreviewListById( (Integer) ((DBRef) item).getId(), index-1 ));
//                else if(((DBRef) item).getCollectionName().equals("body"))
//                    arr.add(getPreviewById( (Integer) ((DBRef) item).getId(), index-1 ));
//            } else {
//                arr.add(item);
//            }
//        });
//
//        return arr;
    }

    private Document getPreviewById(int id, int index) {
        if(index <= 0) return null;

        return bodyMongoRepository.getMappedDocById(id);

//        Document getedDoc = bodyMongoRepository.findOne(query);
//        if(getedDoc == null) return null;
//        Document result = new Document();
//
//        getedDoc.put("id", getedDoc.get("_id"));
//        getedDoc.remove("_id");
//
//        getedDoc.forEach((name, value) -> {
//            if(value instanceof DBRef && ((DBRef) value).getCollectionName().equals("body")) {
//                result.put(name, getPreviewById( (Integer) ((DBRef) value).getId(), index-1 ));
//            } else if(value instanceof DBRef && ((DBRef) value).getCollectionName().equals("lists")){
//                result.put(name, getPreviewListById( (Integer) ((DBRef) value).getId(), index-1 ));
//            } else {
//                result.put(name, value);
//            }
//        });
//
//        return result;
    }

    private Document toDOC(Document value, List<ModelFieldDTO> model) throws IOException {
        Object bodyValue;
        Document dbRef;
        Document result = new Document();

        for(ModelFieldDTO field: model) {
            dbRef = new Document();
            bodyValue = value.get(field.getName());

            if(
                    field.getType().matches(RegexConstants.VALID_TYPE_MODEL) ||
                            field.getType().matches(RegexConstants.VALID_TYPE_MODEL_LIST) ||
                            field.getType().matches(RegexConstants.VALID_TYPE_INTAGER_LIST) ||
                            field.getType().matches(RegexConstants.VALID_TYPE_FLOAT_LIST) ||
                            field.getType().matches(RegexConstants.VALID_TYPE_STRING_LIST) ||
                            field.getType().matches(RegexConstants.VALID_TYPE_BOOLEAN_LIST) ||
                            field.getType().matches(RegexConstants.VALID_TYPE_IMAGE_LIST)
            ) {

                if(bodyValue == null) {
                    result.put(field.getName(), null);
                } else {
                    dbRef.put("$id", bodyValue);
                    if(field.getType().matches(RegexConstants.VALID_TYPE_MODEL)) dbRef.put("$ref", "body");
                    else dbRef.put("$ref", "lists");
                    result.put(field.getName(), dbRef);
                }

                continue;

            }

            switch (FieldTypeEnum.valueOf(field.getType())) {

                case STRING:
                    result.put(field.getName(), bodyValue);
                    break;

                case INTAGER:
                    result.put(field.getName(), (bodyValue != null) ? Integer.valueOf(bodyValue.toString()) : null );
                    break;

                case FLOAT:
                    result.put(field.getName(), (bodyValue != null) ? Double.parseDouble(bodyValue.toString()) : null );
                    break;

                case BOOLEAN:
                    result.put(field.getName(),  (bodyValue != null) ? Boolean.valueOf(bodyValue.toString()) : null );
                    break;

                case IMAGE:
                    result.put(field.getName(), bodyValue.toString());
                    break;

                default:
                    result.put(field.getName(), bodyValue);
                    break;

            }

        }

        return result;
    }

    private List<Object> toArrayDoc(List<Object> docArr, String type) {
        Document dbRef;
        List<Object> arr = new ArrayList<>();

        for(Object value: docArr) {

            if (type.matches(RegexConstants.VALID_TYPE_MODEL)) {
                dbRef = new Document();
                dbRef.put("$id", (Integer) value);
                dbRef.put("$ref", "body");
                arr.add(dbRef);
            } else if (type.matches(RegexConstants.VALID_TYPE_MODEL_LIST)) {
                dbRef = new Document();
                dbRef.put("$id", (Integer) value);
                dbRef.put("$ref", "lists");
                arr.add(dbRef);
            } else {
                arr.add(value);
            }

        }

        return arr;

    }

}
