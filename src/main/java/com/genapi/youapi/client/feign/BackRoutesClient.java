package com.genapi.youapi.client.feign;

import com.genapi.youapi.dto.RouteDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "back")
@RequestMapping("/back/internal/routes")
public interface BackRoutesClient {

    @GetMapping("/view-by-route-and-project")
    RouteDTO getViewByRouteAndProjectId(@RequestParam String route, @RequestParam Long projectId);

}
