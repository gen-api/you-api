package com.genapi.youapi.client;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoDatabase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.UnknownHostException;


@Component
public class MongoDBClient {

    @Value("${spring.data.mongodb.uri}")
    private String dbUrl;

    @Value("${spring.data.mongodb.database}")
    private String dbame;

    private MongoClient client;
    private MongoDatabase database;

    public void connect() throws UnknownHostException {
        client = new MongoClient(new MongoClientURI(dbUrl));
        database = client.getDatabase(dbame);
    }

    public void close() {
        client.close();
    }

    public MongoClient getClient() {
        return client;
    }

    public MongoDatabase getDatabase() {
        return database;
    }

    public ClientSession startSession() {
        return client.startSession();
    }
}
