package com.genapi.youapi.utils;

import com.genapi.youapi.enums.RolesEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Component
public class SecurityUtility {

    @Autowired
    private TokenStore tokenStore;

    public Long getUserId() {
        return Long.valueOf((Integer) getUserDetails().get("id"));
    }

    public String getUserLogin() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    public boolean containsRole(RolesEnum[] roles) {
        List<RolesEnum> authorities = Arrays.asList(getUserRoles());
        for(RolesEnum role: roles) {
            if(authorities.contains(role)) return true;
        }
        return false;
    }

    public boolean containsRole(RolesEnum role) {
        return containsRole(new RolesEnum[]{role});
    }

    public RolesEnum[] getUserRoles() {
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        return authorities.stream().map(item -> RolesEnum.valueOf(item.getAuthority())).toArray(RolesEnum[]::new);
    }

    public Long getUserProjectId() {
        Map<String, Object> details = getUserDetails();
        if(details == null) return null;
        return details.get("projectId") != null ? (long) (int) details.get("projectId") : null;
    }

    public String getUserAuthClient() {
        Map<String, Object> details = getUserDetails();
        if(details == null) return null;
        return details.get("auth_client") != null ? (String) details.get("auth_client") : null;
    }

    public Map<String, Object> getUserDetails() {
        OAuth2AuthenticationDetails details =
                (OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        OAuth2AccessToken accessToken = tokenStore
                .readAccessToken(details.getTokenValue());
        return accessToken.getAdditionalInformation();
    }

    public boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated();
    }

}
