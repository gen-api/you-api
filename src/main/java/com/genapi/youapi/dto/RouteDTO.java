package com.genapi.youapi.dto;

import com.genapi.youapi.enums.ResponseTypeEnum;
import lombok.Data;

@Data
public class RouteDTO {
    private Long id;
    private String name;
    private String route;
    private Long projectId;
    private Long bodyId;
    private Long listId;
    private ResponseTypeEnum responseType;
    private Long projectOwner;

    private boolean auth;
    private boolean active;
}
