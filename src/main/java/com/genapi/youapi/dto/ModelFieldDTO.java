package com.genapi.youapi.dto;

import lombok.Data;

@Data
public class ModelFieldDTO {
    private String name;
    private String type;
    private boolean required;
}
