package com.genapi.youapi.enums;

public enum RolesEnum {

    ADMIN,
    USER,
    PROJECT_USER;

}
