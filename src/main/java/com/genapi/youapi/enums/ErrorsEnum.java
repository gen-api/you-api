package com.genapi.youapi.enums;

public enum ErrorsEnum {

    NOT_VALID,
    NOT_VALID_BODY,
    MATCH_NAME,
    MATCH_LOGIN,
    NOT_MATCH,
    REQUIRED,
    RECURSIVE_PARAMETER,
    REQUIRED_PARAMETER,
    NOT_EDITABLE,
    NO_FIELDS,


    MODEL_BY_PROJECT,
    NOT_MATCH_MODEL,
    MATCH_FIELD_NAME,


    MATCH_ROUTE

}
