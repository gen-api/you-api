package com.genapi.youapi.repository;

import com.genapi.youapi.client.MongoDBClient;
import com.mongodb.*;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public abstract class MongoRepository {

    public MongoDBClient mongoDBClient;

    public String collectionName;

//    private TransactionOptions txnOptions = TransactionOptions.builder()
//            .readPreference(ReadPreference.primary())
//            .readConcern(ReadConcern.LOCAL)
//            .writeConcern(WriteConcern.MAJORITY)
//            .build();

    private MongoDatabase getDataBase() {
        try {
            mongoDBClient.connect();
            return mongoDBClient.getDatabase();
        } catch (UnknownHostException e) {
            return null;
        }
    }

    private MongoCollection<Document> getCollection() {
        MongoDatabase database = getDataBase();
        if(database != null) {
            return database.getCollection(collectionName);
        } else {
            return null;
        }
    }

    public Document findOne(BasicDBObject query) {
        MongoCollection<Document> collection = getCollection();
        if(collection == null) return null;
        return collection.find(query).first();
    }

    public Document findOne(Integer id) {
        BasicDBObject query = new BasicDBObject();
        query.put("_id", id);
        return findOne(query);
    }

    public boolean isExist(BasicDBObject query) {
        MongoCollection<Document> collection = getCollection();
        if(collection == null) return false;
        return collection.countDocuments(query) != 0 ? true : false;
    }

    public Document findOneById(Integer id) {
        BasicDBObject query = new BasicDBObject();
        query.put("_id", id);
        return findOne(query);
    }

    public Document getMappedDoc(BasicDBObject query) {
        MongoDatabase db = getDataBase();
        Document doc = db.runCommand(new Document("$eval", "mapApiDocs('" + collectionName + "', " + query.toJson() + ")"));
        return (Document) ((ArrayList<Object>) doc.get("retval")).get(0);
    }

    public Document getMappedDocById(Integer id) {
        BasicDBObject query = new BasicDBObject();
        query.put("_id", id);
        return getMappedDoc(query);
    }

    public boolean save(Integer id, Document doc) {

        doc.put("_id", id);

        MongoCollection<Document> collection = getCollection();
        if(collection == null) return false;

        BasicDBObject query = new BasicDBObject();

        query.put("_id", id);

        if(isExist(query)) {
            ClientSession clientSession = mongoDBClient.startSession();
            clientSession.startTransaction();

            collection.deleteMany(query);
            collection.insertOne(doc);

            clientSession.commitTransaction();
            return true;
        }

        collection.insertOne(doc);
        return true;
    }

    public boolean delete(BasicDBObject query) {
        MongoCollection<Document> collection = getCollection();
        if(collection == null) return false;
        collection.deleteMany(query);
        return true;
    }

    public boolean delete(List<BasicDBObject> querys) {
        MongoCollection<Document> collection = getCollection();
        ClientSession clientSession = mongoDBClient.startSession();

        if(collection == null) return false;

        clientSession.startTransaction();

        querys.forEach(query -> {
            collection.deleteMany(query);
        });

        clientSession.commitTransaction();

        return true;
    }

    public boolean deleteById(Integer id) {
        BasicDBObject query = new BasicDBObject();
        query.put("_id", id);
        return delete(query);
    }

    public boolean deleteById(List<Integer> ids) {
        List<BasicDBObject> querys = new ArrayList<>();

        ids.forEach(id -> {
            BasicDBObject query = new BasicDBObject();
            query.put("_id", id);
            querys.add(query);
        });

        return delete(querys);
    }

}
