package com.genapi.youapi.repository;

import com.genapi.youapi.client.MongoDBClient;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ListsMongoRepository extends MongoRepository {

    @Autowired
    public ListsMongoRepository(MongoDBClient mongoDBClient) {
        this.mongoDBClient = mongoDBClient;
        this.collectionName = "lists";
    }

    public boolean save(Integer id, List<Object> list) {
        Document doc = new Document();
        doc.put("LIST", list);
        return super.save(id, doc);
    }
}
