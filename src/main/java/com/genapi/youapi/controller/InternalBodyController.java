package com.genapi.youapi.controller;

import com.genapi.youapi.dto.ModelFieldDTO;
import com.genapi.youapi.service.BodyPreviewService;
import lombok.AllArgsConstructor;
import org.bson.Document;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@RequestMapping("/internal/body")
@RestController
public class InternalBodyController {

    private final BodyPreviewService bodyPreviewService;

    @GetMapping("/get-for-body/{id}")
    public Document getByIdForBody(@PathVariable Integer id) {
        return bodyPreviewService.getByIdForBody(id);
    }

    @GetMapping("/get-preview/{id}")
    public Document getPreviewById(@PathVariable Integer id) {
        return bodyPreviewService.getPreviewById(id);
    }

    @PostMapping("/save")
    public Document save(@RequestParam Integer id, @RequestParam String value, @RequestParam String model) throws IOException {
        return bodyPreviewService.save(id, Document.parse(value), Arrays.asList((new ObjectMapper()).readValue(model, ModelFieldDTO[].class)));
    }

    @GetMapping("/get-for-list/{id}")
    public List<Object> getByIdForList(@PathVariable Integer id) {
        return bodyPreviewService.getByIdForList(id);
    }

    @GetMapping("/get-preview-list/{id}")
    public List<Object> getPreviewListById(@PathVariable Integer id) {
        return bodyPreviewService.getPreviewListById(id);
    }

    @PostMapping("/save-list")
    public List<Object> saveList(@RequestParam Integer id, @RequestParam String list, @RequestParam String type) throws IOException {
        return bodyPreviewService.saveList(id, Arrays.asList((new ObjectMapper()).readValue(list, Object[].class)), type);
    }

    @DeleteMapping("/delete-body/{id}")
    public boolean deleteBody(@PathVariable Integer id) {
        return bodyPreviewService.deleteBody(id);
    }

    @DeleteMapping("/delete-body")
    public boolean deleteBody(@RequestParam List<Integer> ids) {
        return bodyPreviewService.deleteBody(ids);
    }

    @DeleteMapping("/delete-list/{id}")
    public boolean deleteList(@PathVariable Integer id) {
        return bodyPreviewService.deleteList(id);
    }

    @DeleteMapping("/delete-list")
    public boolean deleteList(@RequestParam List<Integer> ids) {
        return bodyPreviewService.deleteList(ids);
    }

    /*@GetMapping("/test")
    public ResponseEntity<String> test() throws UnknownHostException {
        return ResponseEntity.ok(bodyPreviewService.test());
    }

    @PostMapping("/test2")
    public void test2(@RequestBody Document json) throws UnknownHostException {
        bodyPreviewService.test2(json);
    }*/

//    @GetMapping("/{id}")
//    public ResponseEntity<Document> getById(@PathVariable Integer id) {
//        return ResponseEntity.ok(bodyPreviewService.getById(id));
//    }

//    @PostMapping("/save")
//    public ResponseEntity<Boolean> save(@RequestBody Document doc) {
//        return ResponseEntity.ok(bodyPreviewService.save(doc));
//    }

}
