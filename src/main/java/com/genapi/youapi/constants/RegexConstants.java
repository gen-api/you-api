package com.genapi.youapi.constants;

public class RegexConstants {

    public static final String VALID_LOGIN = "^\\w{1,50}$";

    public static final String VALID_EMAIL = "^\\w+@\\w+\\.\\w{2,}$";

    public static final String VALID_PASSWORD = "^\\w{5,50}$";

    public static final String VALID_FIELD_NAME = "^\\w{1,50}$";

    public static final String VALID_FIELD_IMAGE = "^data:image\\/[a-z]+;base64,([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$";

    public static final String VALID_TYPE_MODEL = "^MODEL\\(\\d+\\)$";

    public static final String VALID_TYPE_MODEL_LIST = "^MODEL\\(\\d+\\)\\[\\]$";

    public static final String VALID_TYPE_INTAGER_LIST = "^INTAGER\\[\\]$";

    public static final String VALID_TYPE_FLOAT_LIST = "^FLOAT\\[\\]$";

    public static final String VALID_TYPE_STRING_LIST = "^STRING\\[\\]$";

    public static final String VALID_TYPE_BOOLEAN_LIST = "^BOOLEAN\\[\\]$";

    public static final String VALID_TYPE_IMAGE_LIST = "^IMAGE\\[\\]$";

}
