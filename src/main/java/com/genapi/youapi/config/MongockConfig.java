package com.genapi.youapi.config;

import com.github.cloudyrock.mongock.SpringBootMongock;
import com.github.cloudyrock.mongock.SpringBootMongockBuilder;
import com.github.cloudyrock.mongock.SpringMongock;
import com.github.cloudyrock.mongock.SpringMongockBuilder;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MongockConfig {

    @Value("${spring.data.mongodb.uri}")
    private String dbUrl;

    @Value("${spring.data.mongodb.database}")
    private String dbName;

    @Bean
    public SpringBootMongock mongock(ApplicationContext springContext, MongoClient mongoClient) {
        return new SpringBootMongockBuilder(mongoClient, dbName, "com.genapi.youapi.changelogs.mongo")
                .setApplicationContext(springContext)
                .setLockQuickConfig()
                .build();
    }

    /*@Bean
    public SpringBootMongock mongock() {
        MongoClient mongoclient = new MongoClient(new MongoClientURI(dbUrl));
        return new SpringBootMongockBuilder(mongoclient, dbName, "com.genapi.youapi.changelogs.mongo")
                .setLockQuickConfig()
                .build();
    }*/

}
