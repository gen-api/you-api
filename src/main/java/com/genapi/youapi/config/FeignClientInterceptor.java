package com.genapi.youapi.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class FeignClientInterceptor implements RequestInterceptor {

    private static final String AUTHORIZATION_HEADER = "Server-Token";

    private static final String BEARER_TOKEN_TYPE = "Bearer";

    @Value("${spring.security.server-key}")
    private String secret;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public void apply(RequestTemplate template) {
        template.header(AUTHORIZATION_HEADER,String.format("%s %s", BEARER_TOKEN_TYPE, passwordEncoder.encode(secret)));
    }

}
