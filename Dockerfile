FROM openjdk:8
ADD target/you-api.jar you-api.jar
ENV MONGODB_URI=localhost\
    MONGODB_BD=default\
    MONGODB_USER=default\
    MONGODB_PASSWORD=default
EXPOSE 80
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=prod", "you-api.jar"]